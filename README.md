# Juego del 3 en raya

En esta prueba he intentado demostrar mis conocimientos en programación,
he decidido tomarme ciertas libertades con el propósito de demostrar mis conocimientos.

### Pre-requisitos 📋

Esta prueba ha sido desarrollada sobre un contenedor docker, necesitará instalar las dos siguientes herramientas:
- Docker -> https://www.docker.com/
- Git Bash -> ( Windows ) https://gitforwindows.org/

## Comenzando 🚀

Esta prueba ha sido desarrollada sobre un contenedor docker, lo primero que deberá hacer es levantar los contenedores,
sitúese dentro de la carpeta .docker y allí podrá ver y editar el fichero docker-compose.yml, por si fuera necesario
cambiar el puerto en el que escuchan estos contenedores, si está todo correcto ejecute este comando en la consola:
    -> docker-compose up -d --build

### Instalación 🔧 y Despliegue 📦

Una vez levantados los contenedores de docker, deberemos dentro de la carpeta .docker lanzar los
siguientes comandos que nos generarán la base de datos y sus estructura:
    -> docker exec -it game_www bash                * Accederemos al contenedor
    -> cd /var/www/html
    -> php bin/console doctrine:database:create     * Genera la base de datos
    -> php bin/console doctrine:schema:create       * Genera el esquema de la base de datos

## Ejecutando las pruebas ⚙️

Ha llegado el momento de que accedas a 'http://game.local:81/' ó 'http://localhost:81/' y jueges unas partidas al 3 en raya 😉

## Construido con 🛠️

- Docker
- Symfony 4
- JavaScript y JQuery
- Css propio y Bootstrap

## Autor ✒️

Jordán Vicente Gil -> jordanvicentegil@gmailcom