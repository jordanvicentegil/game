<?php

namespace App\Controller;

use App\Entity\Game;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GameController extends AbstractController
{
    /**
    * @Route("/", name="game")
    */
    public function index()
    {
        return $this->render('game/index.html.twig', [
            'title' => 'Juego 3 en raya',
        ]);
    }

    /**
    * @Route("/endGame", options={"expose": true}, methods={"POST"}, name="end_game")
    */
    public function endGame(Request $request) : JsonResponse
    {
        if($request->isXmlHttpRequest()) {
            $entityManager = $this->getDoctrine()->getManager();

            $game = new Game();
            $game->setDate(new \DateTime());
            $game->setNamePlayerOne($request->request->get('playerOne'));
            $game->setNamePlayerTwo($request->request->get('playerTwo'));
            if(!empty($request->request->get('playerWin'))){
                if ($request->request->get('playerWin') === 'x') {
                    $game->setWinPlayerOne(1);
                    $game->setWinPlayerTwo(0);
                } else {
                    $game->setWinPlayerTwo(1);
                    $game->setWinPlayerOne(0);
                }
            }else{
                $game->setWinPlayerOne(0);
                $game->setWinPlayerTwo(0);
            }

            $entityManager->persist($game);
            $entityManager->flush();

            return $this->json([
                'code' => Response::HTTP_OK,
                'message' => Response::$statusTexts[Response::HTTP_OK]
            ]);
        } else {
            return $this->json([
                'code' =>Response::HTTP_BAD_REQUEST,
                'message' => Response::$statusTexts[Response::HTTP_BAD_REQUEST]
            ]);
        }
    }
}
