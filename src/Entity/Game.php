<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name_player_one;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name_player_two;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $date;

    /**
     * @ORM\Column(type="boolean", options={"default": "0"})
     */
    private $win_player_one;

    /**
     * @ORM\Column(type="boolean", options={"default": "0"})
     */
    private $win_player_two;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlayerOne(): ?string
    {
        return $this->player_one;
    }

    public function setPlayerOne(string $player_one): self
    {
        $this->player_one = $player_one;

        return $this;
    }

    public function getPlayerTwo(): ?string
    {
        return $this->player_two;
    }

    public function setPlayerTwo(string $player_two): self
    {
        $this->player_two = $player_two;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getNamePlayerOne(): ?string
    {
        return $this->name_player_one;
    }

    public function setNamePlayerOne(string $name_player_one): self
    {
        $this->name_player_one = $name_player_one;

        return $this;
    }

    public function getNamePlayerTwo(): ?string
    {
        return $this->name_player_two;
    }

    public function setNamePlayerTwo(string $name_player_two): self
    {
        $this->name_player_two = $name_player_two;

        return $this;
    }

    public function getWinPlayerOne(): ?bool
    {
        return $this->win_player_one;
    }

    public function setWinPlayerOne(bool $win_player_one): self
    {
        $this->win_player_one = $win_player_one;

        return $this;
    }

    public function getWinPlayerTwo(): ?bool
    {
        return $this->win_player_two;
    }

    public function setWinPlayerTwo(bool $win_player_two): self
    {
        $this->win_player_two = $win_player_two;

        return $this;
    }
}
