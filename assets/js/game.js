var player = 'x';
var blank = 0, X = 1, O = -1;
var mouse = { x: -1, y: -1 };
var board = [0, 0, 0, 0, 0, 0, 0, 0, 0];
var winBoard = [
    [ 0 , 1 , 2 ], [ 3 , 4 , 5 ], [ 6 , 7 , 8 ],
    [ 0 , 3 , 6 ], [ 1 , 4 , 7 ], [ 2 , 5 , 8 ],
    [ 0 , 4 , 8 ], [ 2 , 4 , 6 ]
]

var canvas = document.getElementById('canvasGame');
var context = canvas.getContext('2d');
var canvasSize = 500;
var sectionSize = canvasSize / 3;
canvas.width = canvasSize;
canvas.height = canvasSize;
context.translate(0.5, 0.5);


canvas.addEventListener('click', function(e) {
    drawPlayerToken(mouse.x, mouse.y);
});

canvas.addEventListener('mouseout', function () {
    mouse.x = mouse.y = -1;
});

canvas.addEventListener('mousemove', function (e) {
    mouse.x = e.pageX - canvas.offsetLeft;
    mouse.y = e.pageY - canvas.offsetTop;
});

$('#btnPlay').click(function (e) {
    var error = false;

    if(($('#inputPlayerOne').val() == '') || (($('#inputPlayerTwo').val() == '') && (!$('#checkIA').is(':checked')))) {
        error = true;

        if ($('#inputPlayerOne').val() == ''){
            $('#inputPlayerOne').attr('required', 'required');
        }

        if ($('#inputPlayerTwo').val() == ''){
            $('#inputPlayerTwo').attr('required', 'required');
        }

        $('#formIntputsGame').addClass('was-validated');
    }


    if(!error) {
        drawBoard();
        $('#inputPlayerOne').attr('disabled', true);
        $('#inputPlayerTwo').attr('disabled', true);
        $('#consoleGame').removeAttr('hidden');
        consoleGame.innerHTML += '##############<br>';
        consoleGame.innerHTML += '#  3 EN RAYA #<br>';
        consoleGame.innerHTML += '##############<br>';
        consoleGame.innerHTML += 'Turno del jugador '+player.toUpperCase()+'<br>';
        $('#canvasGame').removeAttr('hidden');
    }
});

function checkGame (player) {
    for (var i = 0; i < winBoard.length; i++) {
        if ((board[winBoard[i][0]] == player) &&
            (board[winBoard[i][1]] == player) &&
            ( board[winBoard[i][2]] == player)) {
            return true;
        }
    }

    return false;
}// checkGame

function drawBoard() {
    var lineStart = 4;
    var lineWidth = 4;
    var lineColor = '#495057';
    var lineLenght = canvasSize - 5;

    context.lineWidth = lineWidth;
    context.lineCap = 'round';
    context.strokeStyle = lineColor;
    context.beginPath();

    for (var i = 1; i <= 2; i++) {
        context.moveTo(lineStart, i * sectionSize);
        context.lineTo(lineLenght, i * sectionSize);
        context.moveTo(i * sectionSize, lineStart);
        context.lineTo(i * sectionSize, lineLenght);
    }

    context.stroke();
}// drawBoard

function drawPlayerToken(mousePositionX, mousePositionY) {
    var consoleGame = document.getElementById('consoleGame');
    var cell = getCellByCoords(mousePositionX, mousePositionY);
    var xCordinate = '';
    var yCordinate = '';

    for (var x = 0; x < 3; x++) {
        for (var y = 0; y < 3; y++) {
            xCordinate = x * sectionSize;
            yCordinate = y * sectionSize;

            if ( (mousePositionX >= xCordinate) && (mousePositionX <= xCordinate + sectionSize) &&
                (mousePositionY >= yCordinate) && (mousePositionY <= yCordinate + sectionSize) ) {
                if (board[cell] != blank) {
                    return;
                }

                board[cell] = player;
                drawToken(player, xCordinate, yCordinate);

                if (checkGame(player)) {
                    endGame(player);
                    return;
                }
                player = (player === 'x') ? 'o' : 'x';
                consoleGame.innerHTML += 'Turno del jugador '+player.toUpperCase()+'<br>';
            }
        }
    }

    if ((!board.includes(0))) {
        endGame();
    }
}// drawPlayerToken

function drawToken(player, xCordinate, yCordinate) {
    if (player === 'x') {
        var offset = 50;

        context.strokeStyle = '#DB4214';
        context.beginPath();
        context.moveTo(xCordinate + offset, yCordinate + offset);
        context.lineTo(xCordinate + sectionSize - offset, yCordinate + sectionSize - offset);
        context.moveTo(xCordinate + offset, yCordinate + sectionSize - offset);
        context.lineTo(xCordinate + sectionSize - offset, yCordinate + offset);
    } else {
        var halfSectionSize = (0.5 * sectionSize);
        var centerX = xCordinate + halfSectionSize;
        var centerY = yCordinate + halfSectionSize;

        context.strokeStyle = '#7F0AF2';
        context.beginPath();
        context.arc(centerX, centerY, (sectionSize - 100) / 2,  0 * Math.PI, 2 * Math.PI);
    }

    context.stroke();
}// drawToken

function endGame(playerWin) {
    var consoleGame = document.getElementById('consoleGame');
    var URL = Routing.generate('end_game');
    var playerOne = $('#inputPlayerOne').val();
    var playerTwo = $('#inputPlayerTwo').val();

    consoleGame.innerHTML += '#################<br>';
    consoleGame.innerHTML += '# FIN DEL JUEGO #<br>';
    consoleGame.innerHTML += '#################<br>';

    $.ajax({
        data: {
            playerWin: playerWin,
            playerOne: playerOne,
            playerTwo: playerTwo,
        },
        type: 'POST',
        async: true,
        dataType: 'json',
        url: URL,
        success: function(data) {
            var stringWinner = ''
            if ((playerWin != '') && (typeof playerWin != 'undefined')){
                stringWinner = '# Ganador el jugador '+playerWin.toUpperCase()+' #<br>';
            } else {
                stringWinner = '#    ¡ ¡ Empate ! !    #<br>';
            }
            consoleGame.innerHTML += '#######################<br>';
            consoleGame.innerHTML += stringWinner;
            consoleGame.innerHTML += '#######################<br>';
        },
        error: function() {
            consoleGame.innerHTML += '*Error al guardar la información.';
        }
    });

    restarGame();
    drawBoard();
}// endGame

function getCellByCoords(xCordinate, yCordinate) {
    return (Math.floor(xCordinate / sectionSize) % 3) + Math.floor(yCordinate / sectionSize) * 3;
}// getCellByCoords

function getCellCoords(cell) {
    return { 'x': ((cell % 3) * sectionSize), 'y': (Math.floor(cell / 3) * sectionSize)};
}// getCellCoords

function restarGame() {
    context.save();
    context.setTransform(1, 0, 0, 1, 0, 0);
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.restore();

    board = [0, 0, 0, 0, 0, 0, 0, 0, 0];
}// restarGame