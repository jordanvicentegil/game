import '../css/app.css';
import $ from 'jquery';

require('bootstrap');

var Routing = require('../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js');
var Routes = require('../../public/js/fos_js_routes.json');

Routing.setRoutingData(Routes);
global.Routing = Routing;